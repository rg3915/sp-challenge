# sp-challenge

Flask API challenge.

O desafio está descrito em [challenge.md](/blob/master/challenge.md).

## Este projeto foi feito com:

* Python 3.8.2
* Flask
* Docker
* docker-compose

## Como rodar o projeto?

* Clone esse repositório.
* Crie um virtualenv com Python 3.
* Ative o virtualenv.
* Instale as dependências.
* Rode as migrações.

```
git clone https://gitlab.com/rg3915/sp-challenge.git
cd sp-challenge
python3 -m venv .venv
source .venv/bin/activate
pip install -r requirements.txt

cd payment
export FLASK_APP=payment
export FLASK_ENV=development

flask db init
flask db migrate -m "transactions table"
flask db upgrade

flask run
```

Entre em http://localhost:5000/api/doc/
