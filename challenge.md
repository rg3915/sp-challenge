# Challenge: Desenvolvedor Back-End

## Desafio técnico da Shipay

Olá pessoa desenvolvedora! Esperamos que você e sua familia estejam bem!


### Nosso Primeiro Desafio

Nosso principal desafio é a popularização dos meios de pagamento digitais para
os pequenos e médios comerciantes e prestadores de serviço!


Para podermos resolver esse desafio e ajudar essas pessoas, você precisará construir
uma API que será onde ficarão registrados todos os pagamentos realizados por clientes
a um estabelecimento.


O sistema irá receber uma requisição de um PDV (ponto de venda) com as seguintes
informações:


Endpoint de recebimento de transação: `/api/v1/transacao` (POST)



```
# JSON POST:


{
    "estabelecimento": "45.283.163/0001-67",
    "cliente": "094.214.930-01",
    "valor": 590.01,
    "descricao": "Almoço em restaurante chique pago via Shipay!"
}



# Resposta:



{
    "aceito": true
}
```

Já se os dados estiverem inválidos (CNPJ e CPF sem validação, valor em outro formato e etc),
devemos retornar o `aceito` como `false`.


Para também podermos ter os relatórios de transações de um certo estabelecimento, devemos
implementar um endpoint GET onde pegamos todo o listing de transações.


Endpoint: `/api/v1/transacoes/estabelecimento?cnpj=` (parâmetro de CNPJ deve receber um CNPJ)


```
{
    "estabelecimento": {
        "nome": "Nosso Restaurante de Todo Dia LTDA",
        "cnpj": "45.283.163/0001-67",
        "dono": "Fabio I.",
        "telefone": "11909000300",
    },
    "recebimentos": [
        {
            "cliente": "094.214.930-01",
            "valor": 590.01,
            "descricao": "Almoço em restaurante chique pago via Shipay!"
        },
        {
            "cliente": "094.214.930-01",
            "valor": 591,
            "descricao": "Almoço em restaurante chique pago via Shipay!"
        },
    ],
    "total_recebido": 1181.01
}
```

### Nosso Segundo Desafio

O João, seu colega desenvolvedor, solicitou um 'code review' do seguinte script Python,
consegue ajudá-lo?


Você poderá fazer sugestões sobre como facilitar a manutenção do código por outros
integrantes do time, organização do código, segurança, etc. Fique a vontade para sugerir
boas práticas de escrita de códigos (KISS, DRY, SOLID, CleanCode...).


```
import configparser
import logging
# -*- coding: utf-8 -*-
import os
import sys
import traceback
from datetime import datetime, timedelta, timezone
from logging.handlers import RotatingFileHandler

import xlsxwriter
from apscheduler.schedulers.blocking import BlockingScheduler
from flask import Flask
from flask_sqlalchemy import SQLAlchemy


def main(argv):
    greetings()



    print('Press Crtl+{0} to exit'.format('Break' if os.name == 'nt' else 'C'))



    app = Flask(__name__)
    handler = RotatingFileHandler('bot.log', maxBytes=10000, backupCount=1)
    handler.setLevel(logging.INFO)
    app.logger.addHandler(handler)
    app.config['SQLALCHEMY_DATABASE_URI'] = 'postgresql+psycopg2://postgres:123mudar@127.0.0.1:5432/bot_db'
    db = SQLAlchemy(app)
    config = configparser.ConfigParser()
    config.read('/tmp/bot/settings/config.ini')



    var1 = int(config.get('scheduler','IntervalInMinutes'))
    app.logger.warning('Intervalo entre as execucoes do processo: {}'.format(var1))
    scheduler = BlockingScheduler()



    task1_instance = scheduler.add_job(task1(db), 'interval', id='task1_job', minutes=var1)



    try:
        scheduler.start()
    except(KeyboardInterrupt, SystemExit):
        pass



def greetings():
    print('             ##########################')
    print('             # - ACME - Tasks Robot - #')
    print('             # - v 1.0 - 2020-07-28 - #')
    print('             ##########################')



def task1(db):



    file_name = 'data_export_{0}.xlsx'.format(datetime.now().strftime("%Y%m%d%H%M%S"))
    file_path = os.path.join(os.path.curdir, file_name)
    workbook = xlsxwriter.Workbook(file_path)
    worksheet = workbook.add_worksheet()



    orders = db.session.execute('SELECT * FROM users;')



    index = 1



    worksheet.write('A{0}'.format(index),'Id')
    worksheet.write('B{0}'.format(index),'Name')
    worksheet.write('C{0}'.format(index),'Email')
    worksheet.write('D{0}'.format(index),'Password')
    worksheet.write('E{0}'.format(index),'Role Id')
    worksheet.write('F{0}'.format(index),'Created At')
    worksheet.write('G{0}'.format(index),'Updated At')



    for order in orders:
        index = index + 1



        print('Id: {0}'.format(order[0]))
        worksheet.write('A{0}'.format(index),order[0])
        print('Name: {0}'.format(order[1]))
        worksheet.write('B{0}'.format(index),order[1])
        print('Email: {0}'.format(order[2]))
        worksheet.write('C{0}'.format(index),order[2])
        print('Password: {0}'.format(order[3]))
        worksheet.write('D{0}'.format(index),order[3])
        print('Role Id: {0}'.format(order[4]))
        worksheet.write('E{0}'.format(index),order[4])
        print('Created At: {0}'.format(order[5]))
        worksheet.write('F{0}'.format(index),order[5])
        print('Updated At: {0}'.format(order[6]))
        worksheet.write('G{0}'.format(index),order[6])



    workbook.close()
    print('job executed!')



if __name__ == '__main__':
    main(sys.argv)
```


### O que gostariamos de ver?

* Python 3 > (não necessariamente, pode ser feito em outra linguagem)
* Testes bem escritos e com boa cobertura
* Organização e instruções de como executar o projeto (se entregar em docker melhor ainda!)
