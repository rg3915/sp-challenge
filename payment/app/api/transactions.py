from app import db
from app.api import api
from app.models import Establishment, Transaction
from flask import request
from flask_restplus import Resource, abort, fields, reqparse

transaction_model = api.model(
    'Transaction',
    {
        'id': fields.String(readonly=True),
        'establishment': fields.String(required=True, example='45.283.163/0001-67'),
        'customer': fields.String(example='094.214.930-01'),
        'value': fields.Float(example=590.01),
        'description': fields.String(example='Almoço em restaurante chique pago via Shipay!')
    }
)

establishment_model = api.model(
    'Establishment',
    {
        'id': fields.String(readonly=True),
        'name': fields.String(example='Nosso Restaurante de Todo Dia LTDA'),
        'cnpj': fields.String(required=True, example='45.283.163/0001-67'),
        'owner': fields.String(required=True, example='Fabio I.'),
        'phone': fields.String(required=True, example='11909000300'),
    }
)

pagination = api.model(
    'Page Model', {
        'page': fields.Integer(
            description='Number of this page of results'
        ),
        'per_page': fields.Integer(
            description='Number of items per page of results'
        ),
        'total_pages': fields.Integer(
            description='Total number of pages of results'
        ),
        'total_items': fields.Integer(description='Total number of results'),
        'next_page': fields.String(),
        'prev_page': fields.String()
    }
)

transactions_list = api.inherit(
    'Page of transactions',
    pagination,
    {'transactions': fields.List(fields.Nested(transaction_model))}
)

establishments_list = api.inherit(
    'Page of establishments',
    pagination,
    {'establishments': fields.List(fields.Nested(establishment_model))}
)

ns_transactions = api.namespace('transactions', description='Transactions')
ns_establishments = api.namespace(
    'establishments', description='Establishments')

parser = reqparse.RequestParser()
parser.add_argument('page', 1, type=int, location='args', required=False)
parser.add_argument('per_page', 10, choices=[5, 10, 25, 50, 100], type=int,
                    location='args', required=False)
parser.add_argument('cnpj', location='args', required=False)


@ns_transactions.route('/<int:id>')
class TransactionService(Resource):

    @api.marshal_with(transaction_model)
    def get(self, id):
        return Transaction.query.get_or_404(id).to_dict()

    @api.expect(transaction_model)
    @api.marshal_with(transaction_model)
    def put(self, id):
        transaction = Transaction.query.get_or_404(id)
        data = request.get_json() or {}
        if 'transaction' in data and data['establishment'] != transaction.establishment:
            return abort(400, 'please use a different establishment')
        transaction.from_dict(data)
        db.session.commit()
        return transaction.to_dict()

    @api.response(204, 'Transaction deleted')
    def delete(self, id):
        transaction = Transaction.query.get_or_404(id)
        db.session.delete(transaction)
        db.session.commit()
        return 'Success', 204


@ns_transactions.route('/')
class TransactionsService(Resource):

    @api.expect(parser, validate=True)
    @api.marshal_list_with(transactions_list, skip_none=True, code=200)
    def get(self):
        args = parser.parse_args()
        per_page = min(args['per_page'], 100)
        page = args['page']
        query = Transaction.to_collection_dict(
            Transaction.query, page, per_page)
        data = {
            'transactions': query['items'],
            'page': page,
            'per_page': per_page,
            'total_pages': query['_meta']['total_pages'],
            'total_items': query['_meta']['total_items'],
        }
        if query['has_next']:
            data.update(
                {
                    'next_page': api.url_for(
                        TransactionsService,
                        page=page + 1,
                        per_page=per_page
                    )
                }
            )
        if query['has_prev']:
            data.update(
                {
                    'prev_page': api.url_for(
                        TransactionsService,
                        page=page - 1,
                        per_page=per_page
                    )
                }
            )
        return data

    @api.expect(transaction_model)
    @api.marshal_with(transaction_model, code=201)
    def post(self):
        data = request.get_json() or {}
        if 'establishment' not in data:
            return abort(400, 'must include establishment fields')
        transaction = Transaction()
        transaction.from_dict(data)
        db.session.add(transaction)
        db.session.commit()
        return transaction.to_dict(), 201


@ns_transactions.route('/establishment')
class TransactionsService(Resource):

    @api.expect(parser, validate=True)
    @api.marshal_list_with(transactions_list, skip_none=True, code=200)
    def get(self):
        args = parser.parse_args()
        per_page = min(args['per_page'], 100)
        page = args['page']
        cnpj = args['cnpj']
        # FAIL
        # query = Establishment.query.filter()
        query = Transaction.to_collection_dict(
            Transaction.query, page, per_page)
        data = {
            'transactions': query['items'],
            'page': page,
            'per_page': per_page,
            'total_pages': query['_meta']['total_pages'],
            'total_items': query['_meta']['total_items'],
        }
        if query['has_next']:
            data.update(
                {
                    'next_page': api.url_for(
                        TransactionsService,
                        page=page + 1,
                        per_page=per_page
                    )
                }
            )
        if query['has_prev']:
            data.update(
                {
                    'prev_page': api.url_for(
                        TransactionsService,
                        page=page - 1,
                        per_page=per_page
                    )
                }
            )
        return data


@ns_establishments.route('/<int:id>')
class EstablishmentService(Resource):

    @api.marshal_with(establishment_model)
    def get(self, id):
        return Establishment.query.get_or_404(id).to_dict()

    @api.expect(establishment_model)
    @api.marshal_with(establishment_model)
    def put(self, id):
        establishment = Establishment.query.get_or_404(id)
        data = request.get_json() or {}
        if 'establishment' in data and data['name'] != establishment.name:
            return abort(400, 'please use a different establishment')
        establishment.from_dict(data)
        db.session.commit()
        return establishment.to_dict()

    @api.response(204, 'Establishment deleted')
    def delete(self, id):
        establishment = Establishment.query.get_or_404(id)
        db.session.delete(establishment)
        db.session.commit()
        return 'Success', 204


@ns_establishments.route('/')
class EstablishmentsService(Resource):

    @api.expect(parser, validate=True)
    @api.marshal_list_with(establishments_list, skip_none=True, code=200)
    def get(self):
        args = parser.parse_args()
        per_page = min(args['per_page'], 100)
        page = args['page']
        query = Establishment.to_collection_dict(
            Establishment.query, page, per_page)
        data = {
            'establishments': query['items'],
            'page': page,
            'per_page': per_page,
            'total_pages': query['_meta']['total_pages'],
            'total_items': query['_meta']['total_items'],
        }
        if query['has_next']:
            data.update(
                {
                    'next_page': api.url_for(
                        EstablishmentsService,
                        page=page + 1,
                        per_page=per_page
                    )
                }
            )
        if query['has_prev']:
            data.update(
                {
                    'prev_page': api.url_for(
                        EstablishmentsService,
                        page=page - 1,
                        per_page=per_page
                    )
                }
            )
        return data

    @api.expect(establishment_model)
    @api.marshal_with(establishment_model, code=201)
    def post(self):
        data = request.get_json() or {}
        if 'name' not in data:
            return abort(400, 'must include name fields')
        establishment = Establishment()
        establishment.from_dict(data)
        db.session.add(establishment)
        db.session.commit()
        return establishment.to_dict(), 201
