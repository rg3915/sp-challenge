from app import db


class PaginatedAPIMixin(object):

    @staticmethod
    def to_collection_dict(query, page, per_page):
        resources = query.paginate(page, per_page, False)
        data = {
            'items': [item.to_dict() for item in resources.items],
            '_meta': {
                'page': page,
                'per_page': per_page,
                'total_pages': resources.pages,
                'total_items': resources.total
            },
            'has_next': True if resources.has_next else False,
            'has_prev': True if resources.has_prev else False
        }
        return data


class Transaction(PaginatedAPIMixin, db.Model):
    id = db.Column(db.Integer, primary_key=True)
    establishment = db.Column(db.String(18), index=True)
    customer = db.Column(db.String(14))
    value = db.Column(db.Numeric(8, 2))
    description = db.Column(db.Text())

    def to_dict(self):
        data = {
            'id': self.id,
            'establishment': self.establishment,
            'customer': self.customer,
            'value': self.value,
            'description': self.description,
            'accept': True,
        }
        return data

    def from_dict(self, data):
        for field in data:
            setattr(self, field, data[field])


class Establishment(PaginatedAPIMixin, db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(100), index=True)
    cnpj = db.Column(db.String(18), index=True, unique=True)
    owner = db.Column(db.String(100))
    phone = db.Column(db.String(20))

    def to_dict(self):
        data = {
            'id': self.id,
            'name': self.name,
            'cnpj': self.cnpj,
            'owner': self.owner,
            'phone': self.phone,
        }
        return data

    def from_dict(self, data):
        for field in data:
            setattr(self, field, data[field])
