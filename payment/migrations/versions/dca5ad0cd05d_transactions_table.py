"""transactions table

Revision ID: dca5ad0cd05d
Revises: 
Create Date: 2020-08-10 01:59:35.292802

"""
import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = 'dca5ad0cd05d'
down_revision = None
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.create_table('establishment',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('name', sa.String(length=100), nullable=True),
    sa.Column('cnpj', sa.String(length=18), nullable=True),
    sa.Column('owner', sa.String(length=100), nullable=True),
    sa.Column('phone', sa.String(length=20), nullable=True),
    sa.PrimaryKeyConstraint('id')
    )
    op.create_index(op.f('ix_establishment_cnpj'), 'establishment', ['cnpj'], unique=True)
    op.create_index(op.f('ix_establishment_name'), 'establishment', ['name'], unique=False)
    op.create_table('transaction',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('establishment', sa.String(length=18), nullable=True),
    sa.Column('customer', sa.String(length=14), nullable=True),
    sa.Column('value', sa.Numeric(precision=8, scale=2), nullable=True),
    sa.Column('description', sa.Text(), nullable=True),
    sa.PrimaryKeyConstraint('id')
    )
    op.create_index(op.f('ix_transaction_establishment'), 'transaction', ['establishment'], unique=False)
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_index(op.f('ix_transaction_establishment'), table_name='transaction')
    op.drop_table('transaction')
    op.drop_index(op.f('ix_establishment_name'), table_name='establishment')
    op.drop_index(op.f('ix_establishment_cnpj'), table_name='establishment')
    op.drop_table('establishment')
    # ### end Alembic commands ###
